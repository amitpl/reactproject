import './App.css';
import {Header} from "./components/Header";
import {MainContent} from "./components/MainContent";
import {Signup} from "./components/Signup";

function App() {
  return (
    <div className="flex flex-col h-screen">
      <Header/>
        <MainContent>
            <Signup />
        </MainContent>
    </div>
  );
}

export default App;
