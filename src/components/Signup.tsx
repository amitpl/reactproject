import {useReducer, useState} from "react";
import * as yup from "yup";
import {InputElement} from "./InputElement";
import {ReactComponent as EyeHide} from "../eye-hide.svg";
import {ReactComponent as EyeShow} from "../eye-show.svg";
import {Registered} from "./Registered";
import {Button} from "./Button";

export const Signup = () => {

    /**
     * State to store show/hide of password/confirmPassword field value
     */
    const [show, setShow] = useState(false);

    /**
     * State to store validation success and redirect to next view
     */
    const [success, setSuccess] = useState(false);

    /**
     * Yup schema of the form data. This is used for validation of input form data.
     */
    const schema = yup.object().shape({
        firstName: yup.string().required('First Name is required'),
        lastName: yup.string().required('Last Name is required'),
        email: yup.string().email('Email should be a valid email address').required('Email is required'),
        password: yup.string()
            .min(8, 'Password should have at least 8 characters')
            .matches(/[a-z]/, 'Password should have at least one lowercase character')
            .matches(/[A-Z]/, 'Password should have at least one uppercase character')
            .matches(/[a-zA-Z]+[^a-zA-Z\s]+/, 'at least 1 number or special char (@,!,#, etc).'),
        confirmPassword: yup.string().oneOf([yup.ref('password'), null], 'Passwords must match')
    });

    /**
     * Object to hold the error message for each form field.
     */
    const errorMessage = {
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        confirmPassword: "",
    }

    /**
     * Initial Form Values
     */
    const initialFormValues = {
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        confirmPassword: "",
        errors: errorMessage
    };

    /**
     * Reducer to update the formValues.
     */
    const [formValues, setFormValues] = useReducer((currentFormValues: any, newFormValues: any) => ({ ...currentFormValues, ...newFormValues}), initialFormValues);

    /**
     * Spread the form values into individual variables
     */
    const { firstName, lastName, email, password, confirmPassword, errors } = formValues;

    /**
     * Handle form change event
     * @param e The event
     */
    const handleFormChange = (e: { target: { name: any; value: any; }; }) => {
        const {name, value} = e.target;
        setFormValues({[name]: value});
    };


    /**
     * Handling of Form Submission
     */
    const onSubmit = () => {
        schema.validate(formValues, { abortEarly: false })
            .then(() => setSuccess(true))
            .catch(validationError => {
                const fieldErrorMap: Map<string, Array<string>> = new Map();
                const fieldErrorMessage = errorMessage;
                for (let error of validationError.inner) {
                    let perFieldErrors = fieldErrorMap.get(error.path);
                    if (!perFieldErrors) {
                        perFieldErrors = [];
                    }
                    perFieldErrors.push(...error.errors);
                    fieldErrorMap.set(error.path, perFieldErrors);
                }

                // Join all errors for a particular field into a single error message
                for (let [key, val] of fieldErrorMap) {
                    // @ts-ignore
                    fieldErrorMessage[key] = val.join(', ');
                }

                setFormValues({['errors']: fieldErrorMessage});
        })

    }

    const signUp = <>
        <div className="grid grid-cols-3">
            <div/>
            <div className="bg-white rounded-2xl mt-16">
                <div className="pt-8 pb-4 text-center text-xl text-black">Sign Up</div>
                <div className="px-16 py-8">
                    <form id="signupForm">
                        <div className="grid grid-cols-2">
                            <InputElement name='firstName' type='text' value={firstName.value}
                                          onChange={handleFormChange} label='First Name:' placeholder='ex. text'
                                          error={errors.firstName}/>
                            <InputElement name='lastName' type='text' value={lastName}
                                          onChange={handleFormChange} label='Last Name:' placeholder='ex. text'
                                          error={errors.lastName}/>
                        </div>

                        <div className="grid grid-cols-1">
                            <InputElement name='email' type='text' value={email} onChange={handleFormChange}
                                          label='Email:' placeholder='ex. text' error={errors.email}/>
                        </div>

                        <div className="grid grid-cols-1">
                            <InputElement name='password' type={show ? 'text' : 'password'} value={password}
                                          onChange={handleFormChange} label='Password:' placeholder='*********'
                                          error={errors.password}>
                                {show ? <EyeHide onClick={() => setShow(!show)}/> :
                                    <EyeShow onClick={() => setShow(!show)}/>}
                            </InputElement>
                        </div>

                        <div className="grid grid-cols-1">
                            <InputElement name='confirmPassword' type={show ? 'text' : 'password'}
                                          value={confirmPassword} onChange={handleFormChange}
                                          label='Confirm Password:' placeholder='This is my password'
                                          error={errors.confirmPassword}>
                                {show ? <EyeHide onClick={() => setShow(!show)}/> :
                                    <EyeShow onClick={() => setShow(!show)}/>}
                            </InputElement>
                        </div>
                        <Button onClick={onSubmit}>
                            Submit
                        </Button>
                    </form>
                </div>
            </div>
            <div/>
        </div>
    </>

    if (success) {
        return ( <Registered />)
    } else {
        return ( signUp )
    }
}
