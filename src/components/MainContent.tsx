import React from "react";

/**
 * Sets the screen for main content below the header
 */
export const MainContent: React.FC<void> = props => {
    return (
        <div className="bg-gray flex-grow">
            {props.children}
        </div>
    )
}