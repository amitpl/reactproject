import React from "react";

type Props = {
    onClick: ()  => void;
    children: React.ReactNode;
}

export const Button: React.FC<Props> = ({ onClick, children }) => {
    return (
        <div className="p-8 text-center">
            <button
                className="focus:outline-none text-sm bg-blue-dark rounded-full text-white px-16 py-4"
                type="button" onClick={onClick}>{children}
            </button>
        </div>
    )
}
