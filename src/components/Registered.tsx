import {useState} from "react";
import {Signup} from "./Signup";
import {Button} from "./Button";

export const Registered = () => {

    const [tryAgain, setTryAgain] = useState(false);

    return (
        <>
            {tryAgain ? <Signup/> :
                <div className="grid grid-cols-3">
                    <div/>
                    <div>
                        <div className="bg-white rounded-2xl mt-16">
                            <div className="pt-8 pb-4 text-center text-xl text-black font-semibold">You're all set!</div>
                            <Button onClick={() => setTryAgain(true)}>
                                Try Again
                            </Button>
                        </div>
                    </div>
                    <div/>
                </div>
            }
        </>
    )
}