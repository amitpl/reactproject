import React from "react";
import {ErrorMessage} from "./ErrorMessage";

type Props = {
    name: string;
    placeholder?: string;
    type: 'text' | 'number' | 'password';
    value: any;
    onChange: React.ChangeEventHandler<HTMLInputElement>;
    error?: string;
    label: string;
    children?: React.ReactNode;
}

export const InputElement: React.FC<Props> = (
    {
        name,
        placeholder,
        type,
        value,
        onChange,
        error,
        label,
        children
    }
) => {
    return (

        <div className="p-4 relative">
            <div className="text-gray-darkest leading-4 py-3">{label}</div>
            <div>
                <input className="focus:outline-none w-full border-b border-gray-light placeholder-gray-light placeholder-font-bold" type={type} name={name} value={value} onChange={onChange} placeholder={placeholder}/>
                <div className="absolute inset-y-0 right-0 flex items-center pb-4">
                    {children}
                </div>
            </div>
            <ErrorMessage error={error} />
        </div>

    )
}
