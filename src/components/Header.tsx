import {ReactComponent as KandaLogo} from '../logo.svg';

export const Header = () => {

        return (
            <div className="shadow-md p-6 grid grid-cols-1 bg-blue">
                <KandaLogo />
            </div>
        )
}