import React from "react";
import {ReactComponent as WarningIcon} from "../warning.svg";

type Props = {
    error?: string;
}

export const ErrorMessage: React.FC<Props> = ({ error }) => {

    return (

        <>
            {error &&
            <div className="py-2 text-red text-xs grid grid-flow-col auto-cols-max">
                <div className="pr-1"><WarningIcon /></div>
                <p className="break-all absolute px-6">{error}</p>
            </div>}
        </>
    )
}