const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      blue: {
        light: '#85d7ff',
        DEFAULT: '#00206A',
        dark: '#005AEC',
      },
      pink: {
        light: '#ff7ce5',
        DEFAULT: '#ff49db',
        dark: '#ff16d1',
      },
      gray: {
        darkest: '#0F194C',
        dark: '#3c4858',
        DEFAULT: '#F9F9F9',
        light: '#C4C4C4',
        lightest: '#f9fafc',
      },
      red: {
        DEFAULT: '#E83963'
      },
      white: {
        DEFAULT: "#FFFFFF"
      }
    },
    fontFamily: {
      'sans': ['nunito', ...defaultTheme.fontFamily.sans],
      'serif': [...defaultTheme.fontFamily.serif],
      'mono': [...defaultTheme.fontFamily.mono]
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
